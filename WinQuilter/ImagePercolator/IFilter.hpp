#pragma once
namespace WinQuilter
{
	class IFilter
	{
	public:
		IFilter(IFilter* preFilter) : mPreFilter(preFilter)
		{

		}
		virtual ~IFilter() = 0
		{

		}

		void operator()(ImageData::TCImg& image)
		{
			if (mPreFilter != nullptr)
			{
				(*mPreFilter)(image);
			}
			Filter(image);
		}

	private:
		virtual void Filter(ImageData::TCImg& image) = 0;

		IFilter* mPreFilter;
	};
}