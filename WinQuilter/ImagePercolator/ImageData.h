#pragma  once
#include "CImg.h"
#include <memory>
#include <thread>

namespace WinQuilter
{
	class IFilter;

	class ImageData
	{

	public:
		typedef cimg_library::CImg<unsigned char> TCImg;

		ImageData(const char* filePath):
			mDisplayLaunched(false),
			mDisplayThreadPtr(nullptr)
		{
			mCImgPtr = std::make_unique<TCImg>(filePath);
		}

		~ImageData()
		{
			if (mDisplayLaunched && mDisplayThreadPtr != nullptr)
			{
				mDisplayThreadPtr->join();
			}
		}

		void ApplyFilter(IFilter& filter);
		void DisplayWindow(const char* windowName);

	private:
		
		typedef std::unique_ptr<TCImg> TCImgPtr;

		static void DisplayImage(TCImg & image, const char* windowName);



		TCImgPtr mCImgPtr;
		bool mDisplayLaunched;
		std::unique_ptr<std::thread> mDisplayThreadPtr;
	};
}