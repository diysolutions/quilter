#pragma once
#include "IFilter.hpp"
#include "CImg.h"

namespace WinQuilter
{
	class BlurFilter : public IFilter
	{
	public:

		BlurFilter(IFilter* preFilter, float radius) : IFilter(preFilter), mSigma(radius)
		{

		}

	private:
		void Filter(ImageData::TCImg& image) override
		{
		//	CImg(const T *const values, const unsigned int size_x, const unsigned int size_y = 1,
		//		const unsigned int size_z = 1, const unsigned int size_c = 1, const bool is_shared = false)
			cimg_library::CImg<unsigned char> wrappedImage(image, true);
			wrappedImage.blur(mSigma);
		}
		float mSigma;
	};
}