// ImagePercolator.cpp : Defines the entry point for the console application.
//

#include "CImg.h"
using namespace cimg_library;

#include "ImageData.h"
#include "BlurFilter.hpp"
#include "AnisotropicFilter.hpp"
#include "MedianFilter.hpp"


int main(int argc, char** argv)
{
	{
		WinQuilter::ImageData parrotPic("../data/parrot.ppm");

		WinQuilter::BlurFilter blurFilter(nullptr, 4);
		WinQuilter::MedianFilter medianFilter(&blurFilter, 5, 16);
		WinQuilter::AnisotropicFilter anisotropicfilter(&medianFilter, 30);

		parrotPic.ApplyFilter(anisotropicfilter);

		parrotPic.DisplayWindow("Filtered image");
	}
	return 0;
}

int mainCIMG(int argc, char* argv[])
{
	CImg<unsigned char> image("D:\\Extra\\quilter\\WinQuilter\\packages\\CImg\\examples\\img\\parrot.ppm"), visu(500, 400, 1, 3, 0);
	const unsigned char red[] = { 255, 0, 0 }, green[] = { 0, 255, 0 }, blue[] = { 0, 0, 255 };
	image.blur(2.5);
	CImgDisplay main_disp(image, "Click a point"), draw_disp(visu, "Intensity profile");
	while (!main_disp.is_closed() && !draw_disp.is_closed()) {
		main_disp.wait();
		if (main_disp.button() && main_disp.mouse_y() >= 0) {
			const int y = main_disp.mouse_y();
			visu.fill(0).draw_graph(image.get_crop(0, y, 0, 0, image.width() - 1, y, 0, 0), red, 1, 1, 0, 255, 0);
			visu.draw_graph(image.get_crop(0, y, 0, 1, image.width() - 1, y, 0, 1), green, 1, 1, 0, 255, 0);
			visu.draw_graph(image.get_crop(0, y, 0, 2, image.width() - 1, y, 0, 2), blue, 1, 1, 0, 255, 0).display(draw_disp);
		}
	}

	return 0;
}

