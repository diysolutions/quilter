#include "ImageData.h"
#include "IFilter.hpp"

namespace WinQuilter
{


	void ImageData::ApplyFilter(IFilter& filter)
	{
		filter(*mCImgPtr);
	}

	void ImageData::DisplayWindow(const char* windowName)
	{
		if (mDisplayLaunched)
		{
			return;
		}
		mDisplayLaunched = true;
		mDisplayThreadPtr = std::make_unique<std::thread>(DisplayImage, *(this->mCImgPtr), windowName);
	}

	void ImageData::DisplayImage(TCImg & image, const char* windowName)
	{
		cimg_library::CImgDisplay imageRenderer(image, windowName);
		while (!imageRenderer.is_closed())
		{
			imageRenderer.wait();
		}
	}
}