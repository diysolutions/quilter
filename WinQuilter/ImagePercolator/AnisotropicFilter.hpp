#pragma once
#include "IFilter.hpp"
#include "CImg.h"

namespace WinQuilter
{
	class AnisotropicFilter : public IFilter
	{
	public:
		AnisotropicFilter(IFilter* preFilter, float amplitude) : IFilter(preFilter), mAmplitude(amplitude)
		{

		}

	private:
		void Filter(ImageData::TCImg& image) override
		{
			cimg_library::CImg<unsigned char> wrappedImage(image, true);
			wrappedImage.blur_anisotropic(mAmplitude);
		}

		float mAmplitude;
	};
}