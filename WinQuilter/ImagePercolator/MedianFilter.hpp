#pragma once
#pragma once
#include "IFilter.hpp"
#include "CImg.h"

namespace WinQuilter
{
	class MedianFilter : public IFilter
	{
	public:
		MedianFilter(IFilter* preFilter, unsigned int size, float distance) :
			IFilter(preFilter),
			mSize(size),
			mDistance(distance)
		{

		}

	private:
		void Filter(ImageData::TCImg& image) override
		{
			cimg_library::CImg<unsigned char> wrappedImage(image, true);
			wrappedImage.blur_median(mSize, mDistance);
		}

		unsigned int mSize;
		float mDistance;
		
	};
}