﻿using ImageProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WinQuilter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ImageProcessor.ImageFactory imageFactory = new ImageProcessor.ImageFactory();
            ImageFactory loadedIF = imageFactory.Load(@"C:\Users\tecioaca\Desktop\bob.bmp");
            loadedIF = loadedIF.Halftone();
            loadedIF.Save(@"C:\Users\tecioaca\Desktop\bobx.bmp");
        }
    }
}
